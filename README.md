# Youtube Search

This is a HTML page that makes use of OAuth to perform API calls on Youtube. It is able to perform basic autocomplete features when searching for youtube channels, and then display information and videos related to that channel.
You can check it out [here](https://tanyixun.gitlab.io/youtube-search/index.html)